package be.kdg.prog6.common;

public enum EventCatalog {

    HELLO_EVENT,
    PERSON_BORN_EVENT,
    PERSON_MARRIED_EVENT;
}
