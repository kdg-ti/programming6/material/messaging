package be.kdg.prog6.receiver;

import be.kdg.prog6.common.EventCatalog;
import be.kdg.prog6.common.EventMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RestReceiver {


    private final ObjectMapper objectMapper;

    public RestReceiver(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @RabbitListener(queues = RabbitMQTopologyConfig.MY_HELLO_QUEUE, messageConverter = "#{jackson2JsonMessageConverter}")
    public void sayHello(EventMessage message) throws JsonProcessingException {
        System.out.println(message);

        /**
         * check here if the event message is a duplicate (using UUID)
         * check if you are interested in this event
         * If so map it to your own understanding of the message
         */

        if (message.getEventHeader().getEventType() == EventCatalog.HELLO_EVENT){
            System.out.println(objectMapper.readValue(message.getEventBody(), HelloMessage.class));
        } else {
            //ignoring other events
        }


    }



}
