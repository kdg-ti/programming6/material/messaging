package be.kdg.prog6.receiver;

import java.io.Serializable;

public class HelloMessage implements Serializable {

    private String name;


    public HelloMessage() {
    }

    public HelloMessage(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }


    @Override
    public String toString() {
        return "HelloMessage{" +
                "name='" + name + '\'' +
                '}';
    }
}
