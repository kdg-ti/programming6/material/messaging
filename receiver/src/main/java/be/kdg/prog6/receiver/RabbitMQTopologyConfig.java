package be.kdg.prog6.receiver;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQTopologyConfig {


    public static final String MY_EXCHANGE_TOPIC = "my-exchange-topic";
    public static final String MY_HELLO_QUEUE = "my-hello-queue";
    public static final String MY_SOMETHING_QUEUE = "my-something-queue";


    @Bean
    TopicExchange myExchangeTopic(){
        return new TopicExchange(MY_EXCHANGE_TOPIC);
    }
    
    @Bean
    Queue mySayHelloQueue(){
        return new Queue(MY_HELLO_QUEUE);
    }

    @Bean
    Queue mySomethingQueue(){
        return new Queue(MY_SOMETHING_QUEUE);
    }


    @Bean
    Binding bindHelloQueueToTopic(TopicExchange myExchangeTopic, Queue mySayHelloQueue){
        return BindingBuilder.bind(mySayHelloQueue).to(myExchangeTopic).with("say.hello.*");
    }

    @Bean
    Binding bindSomethingQueueToTopic(TopicExchange myExchangeTopic, Queue mySomethingQueue){
        return BindingBuilder.bind(mySomethingQueue).to(myExchangeTopic).with("say.something.*");
    }



    @Bean
    Jackson2JsonMessageConverter jackson2JsonMessageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    
    
    
}
