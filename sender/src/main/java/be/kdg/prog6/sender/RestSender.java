package be.kdg.prog6.sender;

import be.kdg.prog6.common.EventCatalog;
import be.kdg.prog6.common.EventHeader;
import be.kdg.prog6.common.EventMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.jfr.Event;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class RestSender {

    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    public RestSender(RabbitTemplate rabbitTemplate, ObjectMapper objectMapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.objectMapper = objectMapper;
    }

    @PostMapping("/hello/{name}")
    public void sayHello(@PathVariable String name) throws JsonProcessingException {
        //stuur mij een berichtje asynchroon op rabbitmq
        System.out.println(new HelloMessage(name));
        EventHeader eventHeader = EventHeader.builder().eventID(UUID.randomUUID()).eventCatalog(EventCatalog.HELLO_EVENT).build();
        EventMessage eventMessage = EventMessage.builder().eventHeader(eventHeader).eventBody(objectMapper.writeValueAsString(new HelloMessage(name))).build();
        rabbitTemplate.convertAndSend(RabbitTopologyConfig.MY_EXCHANGE_TOPIC, "say.hello.name", eventMessage);
    }


}
