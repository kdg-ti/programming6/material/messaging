package be.kdg.prog6.sender;

import java.io.Serializable;

public class HelloMessage implements Serializable {

    private final String name;

    private final String ignoredByReceiver = "ignored";


    public HelloMessage(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public String getIgnoredByReceiver() {
        return ignoredByReceiver;
    }

    @Override
    public String toString() {
        return "HelloMessage{" +
                "name='" + name + '\'' +
                ", ignoredByReceiver='" + ignoredByReceiver + '\'' +
                '}';
    }
}
